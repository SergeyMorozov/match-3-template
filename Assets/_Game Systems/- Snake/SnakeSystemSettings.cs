using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class SnakeSystemSettings : ScriptableObject
    {
        public int StartBodyCount;
        public int MaxBodyCount;
        public float BodyPartDistance;
        public float FoodMoveSpeed;
        public List<SnakePreset> Snakes;
    }
}
