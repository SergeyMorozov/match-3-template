using UnityEngine;

namespace GAME
{
    public class SnakeTrigger : MonoBehaviour
    {
        private SnakeObject _snake;

        private void Awake()
        {
            _snake = GetComponentInParent<SnakeObject>();
        }

        private void OnTriggerEnter(Collider other)
        {
            ItemObject item = other.GetComponentInParent<ItemObject>();
            if(item == null || !item.IsActive) return;
            
            SnakeSystem.Events.SnakeDetectItem?.Invoke(_snake, item);
        }
    }
}

