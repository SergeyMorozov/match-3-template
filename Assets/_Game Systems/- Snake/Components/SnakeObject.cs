using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class SnakeObject : MonoBehaviour
    {
        public SnakePreset Preset;
        public SnakeSkin Skin;

        [Space]
        public Vector3 Position;
        public Vector3 MoveDirect;
        public float MoveValue;
        public float CurrentSpeedMove;

        public List<Transform> BodyParts;
        public List<PathPoint> Path;
        public List<ItemObject> Foods;
    }

    public struct PathPoint
    {
        public Vector3 Position;
        public Quaternion Rotation;
    }
}

