using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class SnakeSkin : MonoBehaviour
    {
        public Animator Animator;
        public Transform CameraDirect;
        public Transform SnakeDirect;
        public Collider FoodDetector;
        public Transform Head;
        public List<Transform> BodyParts;
    }
}

