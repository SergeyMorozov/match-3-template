using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class SnakePreset : ScriptableObject
    {
        public string Name;
        public Sprite Icon;
        public SnakeSkin Skin;

        public float MaxSpeedRotation;
        public float MaxSpeedMove;

        public List<ItemPreset> Foods;
    }
}

