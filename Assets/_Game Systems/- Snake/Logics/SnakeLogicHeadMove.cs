using UnityEngine;

namespace GAME
{
    public class SnakeLogicHeadMove : MonoBehaviour
    {
        private LevelObject _level;

        private void Awake()
        {
            _level = LevelSystem.Data.CurrenLevel;
            SnakeSystem.Events.HeadMove += HeadMove;
        }

        private void HeadMove(SnakeObject snake)
        {
            // Вычисление позиции для перемещения
            snake.CurrentSpeedMove = snake.Preset.MaxSpeedMove;
            snake.Position += snake.transform.forward * Time.deltaTime * snake.CurrentSpeedMove;

            // Выравнивание по поверхности
            Vector3 directToCenter = _level.Skin.CenterPoint.position - snake.Position;
            Ray _ray = new Ray(snake.Position - directToCenter.normalized, directToCenter);
            if (Physics.Raycast(_ray, out var hit, 10)) snake.Position = hit.point;

            // Перемещение
            snake.transform.position = Vector3.Lerp(snake.transform.position, snake.Position, Time.deltaTime * 10);
        }
    }
}
