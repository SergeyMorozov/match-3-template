using System;
using UnityEngine;

namespace GAME
{
    public class SnakeLogicFood : MonoBehaviour
    {
        private void Awake()
        {
            SnakeSystem.Events.SnakeDetectItem += DetectItem;
        }

        private void DetectItem(SnakeObject snake, ItemObject item)
        {
            if (!snake.Preset.Foods.Contains(item.Data.Preset)) return;

            item.IsActive = false;
            item.Skin.Collider.enabled = false;
            snake.Foods.Add(item);
            SnakeSystem.Events.EatingFood?.Invoke(snake, item);
        }

        private void Update()
        {
            foreach (SnakeObject snake in SnakeSystem.Data.Snakes)
            {
                for (var i = 0; i < snake.Foods.Count; i++)
                {
                    var food = snake.Foods[i];
                    food.transform.localPosition = Vector3.MoveTowards(food.transform.position, snake.Skin.Head.position,
                        Time.deltaTime * SnakeSystem.Settings.FoodMoveSpeed);

                    if ((food.transform.position - snake.Skin.Head.position).magnitude > 0.1f) continue;
                    
                    SnakeEatFood(snake, food);
                    i--;
                }
            }
        }

        private void SnakeEatFood(SnakeObject snake, ItemObject item)
        {
            // Удаление еды
            ItemSystem.Events.RemoveItem?.Invoke(item);
            snake.Foods.Remove(item);
            
            // Добавление нового сегмента тела змеи
            SnakeSystem.Events.AddBody?.Invoke(snake);
        }
    }
}

