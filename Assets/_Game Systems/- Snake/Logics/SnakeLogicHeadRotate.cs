using UnityEngine;

namespace GAME
{
    public class SnakeLogicHeadRotate : MonoBehaviour
    {
        private LevelObject _level;
        private Vector3 _pointTarget;

        private void Awake()
        {
            _level = LevelSystem.Data.CurrenLevel;
            SnakeSystem.Events.HeadRotate += HeadRotate;
        }

        private void HeadRotate(SnakeObject snake)
        {
           if (snake.MoveDirect != Vector3.zero)
            {
                // Поворот в сторону заданного направления движения
                Quaternion cameraRotation = snake.Skin.CameraDirect.rotation;
                snake.Skin.SnakeDirect.localRotation = Quaternion.LookRotation(snake.MoveDirect);
                snake.transform.rotation = Quaternion.RotateTowards(snake.transform.rotation,
                    snake.Skin.SnakeDirect.rotation, Time.deltaTime * snake.Preset.MaxSpeedRotation);
                snake.Skin.CameraDirect.rotation = cameraRotation;
            }

            // Выравнивание положения относительно поверхности
            Vector3 directToCenter = _level.Skin.CenterPoint.position - snake.transform.position;
            Ray _ray = new Ray(snake.transform.position + snake.transform.forward - directToCenter.normalized, directToCenter);
            if (Physics.Raycast(_ray, out var hit, 10)) _pointTarget = hit.point;
            snake.transform.LookAt(_pointTarget, -directToCenter);
            Quaternion targetRotation = Quaternion.LookRotation(snake.transform.forward, -directToCenter);
            snake.transform.rotation = Quaternion.Lerp(snake.transform.rotation, targetRotation, Time.deltaTime * 5);
        }

    }
}
