using UnityEngine;

namespace GAME
{
    public class SnakeLogicInputDirect : MonoBehaviour
    {
        private SnakeObject _snakePlayer;
        private Vector3 _pointCenter;

        private void Awake()
        {
        }

        private void Update()
        {
            if (_snakePlayer == null)
            {
                if(SnakeSystem.Data.Snakes.Count == 0) return;
                _snakePlayer = SnakeSystem.Data.Snakes[0];
            }
            
            float enter = 0.0f;

            if (Input.GetMouseButtonDown(0))
            {
                _pointCenter = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                Vector3 delta = (_pointCenter - Input.mousePosition) / Screen.width * 4;
                _snakePlayer.MoveDirect = new Vector3(-delta.x, 0, -delta.y);
                _snakePlayer.MoveDirect.Normalize();
                _snakePlayer.MoveValue = Mathf.Clamp01(delta.magnitude);
            }

            if (Input.GetMouseButtonUp(0))
            {
                
            }
            
            if(Input.GetKeyDown(KeyCode.A))
            {
                SnakeSystem.Events.AddBody?.Invoke(_snakePlayer);
            }
        }
    }
}

