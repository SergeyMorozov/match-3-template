using UnityEngine;

namespace GAME
{
    public class SnakeLogicAnimation : MonoBehaviour
    {
        private void Awake()
        {
            SnakeSystem.Events.EatingFood += EatFood;
        }

        private void EatFood(SnakeObject snake, ItemObject food)
        {
            snake.Skin.Animator.SetTrigger("Eating");
        }

    }
}

