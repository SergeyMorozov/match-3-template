using UnityEngine;

namespace GAME
{
    public class SnakeLogicBodyMove : MonoBehaviour
    {
        private float _distanceToHead;
        private float _bodyPartDistance;
        // private SnakeObject _snakeGizmos;

        private void Awake()
        {
            SnakeSystem.Events.BodyMove += BodyMove;
        }

        private void BodyMove(SnakeObject snake)
        {
            BuildPath(snake);   // Построение списка точек, для перемещения частей тела
            MoveBody(snake);    // Перемещение частей тела, на основе текущего списка точек 
        }

        private void BuildPath(SnakeObject snake)
        {
            // _snakeGizmos = snake;
            _bodyPartDistance = SnakeSystem.Settings.BodyPartDistance;
            if (snake.Path.Count == 0) AddPathPoint(snake);

            // Вычисление дистанции от первой точки до головы
            _distanceToHead = (snake.transform.position - snake.Path[0].Position).magnitude;
            if (_distanceToHead < _bodyPartDistance) return;

            // Добавление новой точки пути
            _distanceToHead -= _bodyPartDistance;
            Vector3 directToHead = (snake.transform.position - snake.Path[0].Position).normalized;

            snake.Path.Insert(0, new PathPoint
                {
                    Position = snake.Path[0].Position + directToHead * _bodyPartDistance,
                    Rotation = snake.transform.rotation
                });

            // Удаление лишних точек
            if (snake.Path.Count > snake.BodyParts.Count + 2)
            {
                snake.Path.RemoveAt(snake.Path.Count - 1);
            }
        }

        private void MoveBody(SnakeObject snake)
        {
            for (int i = 0; i < snake.BodyParts.Count; i++)
            {
                if(i > snake.Path.Count - 2) continue;
                
                Transform body = snake.BodyParts[i];
                body.position = Vector3.Lerp(snake.Path[i + 1].Position, snake.Path[i].Position, _distanceToHead / _bodyPartDistance );
                body.rotation = Quaternion.Lerp(snake.Path[i + 1].Rotation, snake.Path[i].Rotation, _distanceToHead / _bodyPartDistance );
            }
        }

        private void AddPathPoint(SnakeObject snake)
        {
            snake.Path.Add(new PathPoint {Position = snake.transform.position, Rotation = snake.transform.rotation});
        }
        
        /*private void OnDrawGizmos()
        {
            if(_snakeGizmos == null) return;
            
            Gizmos.color = Color.red;
            
            foreach (PathPoint pathPoint in _snakeGizmos.Path)
            {
                Gizmos.DrawSphere(pathPoint.Position, 0.2f);
            }
        }*/
    }
}
