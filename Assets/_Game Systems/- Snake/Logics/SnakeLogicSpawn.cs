using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class SnakeLogicSpawn : MonoBehaviour
    {
        private void Awake()
        {
            SnakeSystem.Events.SpawnSnake += SpawnSnake;
            SnakeSystem.Events.AddBody += AddBody;
        }

        private void SpawnSnake(Transform spawnPoint)
        {
            // Создание змеи
            SnakePreset snakePreset = SnakeSystem.Settings.Snakes[0];
            SnakeObject snake = new GameObject(snakePreset.Name).AddComponent<SnakeObject>();
            snake.Position = snake.transform.position = spawnPoint.position;
            snake.transform.rotation = spawnPoint.rotation;
            snake.Preset = snakePreset;
            snake.Skin = Instantiate(snakePreset.Skin, snake.transform);
            snake.Skin.name = snake.name + " Skin";
            snake.Foods = new List<ItemObject>();

            GameObject bodyPrefab = snake.Skin.BodyParts[0].gameObject;
            bodyPrefab.SetActive(false);
            snake.Path = new List<PathPoint>();
            snake.BodyParts = new List<Transform>();
            
            // Добавление начального количества сегментов
            for (int i = 0; i < SnakeSystem.Settings.StartBodyCount; i++)
            {
                AddBody(snake);
            }

            SnakeSystem.Data.Snakes.Add(snake);

            // Подключение камеры к первой змее
            if(SnakeSystem.Data.Snakes.Count == 1)
                CameraSystem.Events.SetTarget?.Invoke(snake.Skin.CameraDirect);
        }

        private void AddBody(SnakeObject snake)
        {
            GameObject bodyPrefab = snake.Skin.BodyParts[0].gameObject;
            GameObject body = Instantiate(bodyPrefab, snake.transform);
            body.SetActive(true);
            snake.BodyParts.Add(body.transform);
        }

    }
}

