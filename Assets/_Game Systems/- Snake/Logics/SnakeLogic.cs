using UnityEngine;

namespace GAME
{
    public class SnakeLogic : MonoBehaviour
    {
        private void Awake()
        {
        }

        private void Update()
        {
            foreach (SnakeObject snake in SnakeSystem.Data.Snakes)
            {
                SnakeSystem.Events.HeadRotate?.Invoke(snake);
                SnakeSystem.Events.HeadMove?.Invoke(snake);
                SnakeSystem.Events.BodyMove?.Invoke(snake);
            }
        }
    }
}

