using UnityEngine;

namespace GAME
{
    public class SnakeSystem : MonoBehaviour
    {
        private static SnakeSystem Instance;

        private static void CheckInstance()
        {
            if (Instance == null)
                Instance = FindObjectOfType<SnakeSystem>();
        }

        public static SnakeSystemSettings Settings
        {
            get
            {
                CheckInstance();
                return Instance.settings ?? (Instance.settings = new SnakeSystemSettings());
            }
        }

        public static SnakeSystemData Data
        {
            get
            {
                CheckInstance();
                return Instance.data ?? (Instance.data = new SnakeSystemData());
            }
        }

        public static SnakeSystemEvents Events
        {
            get
            {
                CheckInstance();
                return Instance.events ?? (Instance.events = new SnakeSystemEvents());
            }
        }

        [SerializeField] private SnakeSystemSettings settings;
        [SerializeField] private SnakeSystemData data;
        private SnakeSystemEvents events;

    }
}

