using System;
using UnityEngine;

namespace GAME
{
    [Serializable]
    public class SnakeSystemEvents
    {
        public Action<Transform> SpawnSnake;
        public Action<SnakeObject> HeadRotate;
        public Action<SnakeObject> HeadMove;
        public Action<SnakeObject> BodyMove;
        public Action<SnakeObject> AddBody;
        public Action<SnakeObject, ItemObject> SnakeDetectItem;
        public Action<SnakeObject, ItemObject> EatingFood;
    }
}
