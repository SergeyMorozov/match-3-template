using System;
using UnityEngine;

namespace GAME
{
    [Serializable]
    public class CameraSystemEvents
    {
        public Action<Transform> SetTarget;
    }
}
