using UnityEngine;

namespace  GAME
{
    public class CameraLogic : MonoBehaviour
    {
        private CameraObject _camera;
        private Vector3 _targetPoint1;
        private Vector3 _targetPoint2;
        private float _cameraHeight;
        
        private void Awake()
        {
            _camera = CameraSystem.Data.CurrentCamera;
            
            if (_camera.Target != null && _camera.LookAt != null)
                _cameraHeight = (_camera.Target.position - _camera.LookAt.position).magnitude;
            
            CameraSystem.Events.SetTarget += SetTarget;
        }

        private void SetTarget(Transform obj)
        {
            _camera.Target = obj;
            _cameraHeight = (_camera.Target.position - _camera.LookAt.position).magnitude;
        }

        private void LateUpdate()
        {
            if (_camera.Target != null)
            {
                _cameraHeight = Mathf.Lerp(_cameraHeight, (_camera.Target.position - _camera.LookAt.position).magnitude, Time.deltaTime);
                
                _targetPoint1 = _camera.Target.position;
                _targetPoint1 = (_targetPoint1 - _camera.LookAt.position).normalized * _cameraHeight;
                _targetPoint2 = _camera.Target.position + _camera.Target.forward;
                _targetPoint2 = (_targetPoint2 - _camera.LookAt.position).normalized * _cameraHeight;
                
                _camera.transform.position = _targetPoint1;
            }
            
            if (_camera.LookAt != null && _targetPoint1 != _targetPoint2)
            {
                Vector3 up = _targetPoint1 - _camera.LookAt.position;
                Quaternion targetRotation = Quaternion.LookRotation(_targetPoint2 - _targetPoint1, up);

                _camera.transform.rotation = targetRotation;
            }
        }
    }
}

