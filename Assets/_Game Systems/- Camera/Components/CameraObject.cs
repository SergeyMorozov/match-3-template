using UnityEngine;

namespace GAME
{
    public class CameraObject : MonoBehaviour
    {
        public CameraPreset Preset;
        public CameraSkin Skin;

        [Space]
        public Transform Target;
        public Transform LookAt;
    }
}

