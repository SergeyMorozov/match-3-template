using System;
using UnityEngine.Serialization;

namespace GAME
{
    [Serializable]
    public class CameraSystemData
    {
        public CameraObject CurrentCamera;
    }
}
