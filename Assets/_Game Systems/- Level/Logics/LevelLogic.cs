using UnityEngine;
using Random = UnityEngine.Random;

namespace  GAME
{
    public class LevelLogic : MonoBehaviour
    {
        private LevelObject _level;
        
        private void Awake()
        {
            _level = LevelSystem.Data.CurrenLevel;
        }

        private void Start()
        {
            // Спавн еды
            ItemPreset itemPreset = ItemSystem.Settings.Items[0]; 
            ItemSystem.Events.SpawnItems?.Invoke(itemPreset, ItemSystem.Settings.MaxItemsOnLevel);
            
            // Спавн змеи
            Transform spawnPoint = _level.Skin.SpawnSnakePoints[Random.Range(0, _level.Skin.SpawnSnakePoints.Count)];
            SnakeSystem.Events.SpawnSnake?.Invoke(spawnPoint);
        }
    }
}

