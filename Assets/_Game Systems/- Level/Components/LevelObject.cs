using UnityEngine;
using UnityEngine.Serialization;

namespace GAME
{
    public class LevelObject : MonoBehaviour
    {
        public LevelPreset Preset;
        public LevelSkin Skin;
    }
}

