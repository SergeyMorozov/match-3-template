using UnityEngine;

namespace GAME
{
    public class LevelPreset : ScriptableObject
    {
        public string Name;
        public Sprite Icon;
        public LevelSkin Prefab;
        public float RadiusBound;
    }
}

