using Unity.VisualScripting;
using UnityEngine;

namespace  GAME
{
    public class ItemLogic : MonoBehaviour
    {
        private void Awake()
        {
            ItemSystem.Events.RemoveItem += RemoveItem;
        }

        // Удаление еды после использования
        private void RemoveItem(ItemObject item)
        {
            ItemSystem.SystemData.Items.Remove(item);
            Destroy(item.GameObject());
            
            if (ItemSystem.SystemData.Items.Count < ItemSystem.Settings.MaxItemsOnLevel)
            {
                int count = ItemSystem.Settings.MaxItemsOnLevel - ItemSystem.SystemData.Items.Count;
                ItemSystem.Events.SpawnItems?.Invoke(item.Data.Preset, count);
            }
        }
    }
}

