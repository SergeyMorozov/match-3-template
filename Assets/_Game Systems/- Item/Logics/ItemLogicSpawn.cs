using UnityEngine;

namespace  GAME
{
    public class ItemLogicSpawn : MonoBehaviour
    {
        private LevelObject _level;
        
        private void Awake()
        {
            _level = LevelSystem.Data.CurrenLevel;
            
            ItemSystem.Events.SpawnItems += SpawnItems;
        }

        private void SpawnItems(ItemPreset itemPreset, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Vector3 pointSpawn = Random.insideUnitSphere.normalized * _level.Preset.RadiusBound;
                Ray ray = new Ray(pointSpawn, _level.Skin.CenterPoint.position - pointSpawn);
                Physics.Raycast(ray, out var hit, _level.Preset.RadiusBound / 2);
                
                ItemObject item = new GameObject().AddComponent<ItemObject>();
                item.Data = new ItemData { Preset = itemPreset, Value = 1 };
                item.name = itemPreset.name;
                item.Skin = Instantiate(itemPreset.Skin, item.transform);
                item.transform.position = hit.point;
                item.transform.up = hit.normal;
                item.IsActive = true;
                ItemSystem.SystemData.Items.Add(item);
            }
        }
    }
}

