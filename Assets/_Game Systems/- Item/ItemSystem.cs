using UnityEngine;
using UnityEngine.Serialization;

namespace GAME
{
    public class ItemSystem : MonoBehaviour
    {
        private static ItemSystem Instance;

        private static void CheckInstance()
        {
            if (Instance == null)
                Instance = FindObjectOfType<ItemSystem>();
        }

        public static ItemSystemSettings Settings
        {
            get
            {
                CheckInstance();
                return Instance.settings ?? (Instance.settings = new ItemSystemSettings());
            }
        }

        public static ItemSystemData SystemData
        {
            get
            {
                CheckInstance();
                return Instance.data ?? (Instance.data = new ItemSystemData());
            }
        }

        public static ItemSystemEvents Events
        {
            get
            {
                CheckInstance();
                return Instance.events ?? (Instance.events = new ItemSystemEvents());
            }
        }

        [SerializeField] private ItemSystemSettings settings;
        [FormerlySerializedAs("systemData")] [SerializeField] private ItemSystemData data;
        private ItemSystemEvents events;

    }
}

