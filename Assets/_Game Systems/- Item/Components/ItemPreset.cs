using UnityEngine;

namespace GAME
{
    public class ItemPreset : ScriptableObject
    {
        public string Name;
        public Sprite Icon;
        public ItemSkin Skin;
    }
}

