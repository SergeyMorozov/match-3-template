using System;
using UnityEngine;

namespace GAME
{
    public class ItemObject : MonoBehaviour
    {
        public ItemData Data;
        public ItemSkin Skin;
        public bool IsActive;
    }

    [Serializable]
    public class ItemData
    {
        public ItemPreset Preset;
        public float Value;
        public float ValueMax = -1;
    }
}

