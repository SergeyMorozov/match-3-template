using System;

namespace GAME
{
    [Serializable]
    public class ItemSystemEvents
    {
        public Action<ItemPreset, int> SpawnItems;
        public Action<ItemObject> RemoveItem;
    }
}
