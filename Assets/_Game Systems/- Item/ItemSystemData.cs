using System;
using System.Collections.Generic;

namespace GAME
{
    [Serializable]
    public class ItemSystemData
    {
        public List<ItemObject> Items;
    }
}
