using System.Collections.Generic;
using UnityEngine;

namespace GAME
{
    public class ItemSystemSettings : ScriptableObject
    {
        public int MaxItemsOnLevel;
        public List<ItemPreset> Items;
    }
}
